# NAME_FIELD

## L1 Implementation

### L1_NAME_FIELD
L1_DETAIL

#### Request:
L1_REQ_FIELD


#### Response:
L1_RESPONSE_FIELD

---

## L2 Implementation
#### Path :
```Ambrosus-L2-GW/app/src/main/java/com/crayonic/ambrosus/gateway/comm/bt/ble_manager/model```
#### Class :
```CLASS_FIELD```

---

#### Received data format :
DETAILS_OF_DATA_FORMAT_FIELD

---

## Integration
Already integrated in class ```INTEGRATED_IN_CLASS_FILED```. Received data are converted to RESULT_FORMAT.

## Usage
See the steps and example in [Usage of APDU Model](../README.md#usage-of-apdu-model)
