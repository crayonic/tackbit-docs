# APDU Status Words

## Success

| 2B hex | Short name | Detail | 
| --- | ---| --- |
| `9000` | OK | No further qualification |
| `90FF` | OK-Wait | OK followed by APDU transmission from receiving side |
| `7000` | Success | Proprietary code : unspecified success |

## Error

| 2B hex | Short name | Detail |
| --- | ---| --- |
| `6982` | Low Security | Security status not satisfied |
| `6400` | Timeout | Task took too long to execute |
| `6401` | User Abort | User aborted |
| `6982` | Low Security | Security status not satisfied |
| `6983` | Auth Blocked | Authentication method blocked |
| `6B00` | Params Err | Wrong Parameters |
| `6300` | Warning | Unspecified warning |
| `6B00` | Error | Unspecified error |
| `6C00` | Wrong Length | Wrong data length |
| `6700` | Wrong Key Length | Wrong encryption key length | 
| `7FFF` | Error | Proprietary code : Unspecified error |
| `7F00` | Encryption Error | Proprietary code : Failed to perform encryption |
| `7100` | Unexpected Input | Proprietary code : Unexpected input in APDU read/write |
| `FFFF` | Application Error | Proprietary code : Internal error of L2 application |