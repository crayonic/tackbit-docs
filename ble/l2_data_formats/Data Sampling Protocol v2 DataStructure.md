#Delta logging of variety of variably measured types.

Previously we have sampled all types as often as we could at preset sampling interval while saving some power in between samples i.e. 2 second sampling interval made CPU sleep for 1,9 seconds and sample sensors for 0,1 seconds. All sampled data were written in a fixed 16  byte format every 2 seconds inside the buffer.

To achieve much better storage performance while increasing precision of measured data we will keep sampling of all sensors like above however we will only store differences in between current and last written sampled data. The deltas will be stored as part of a PAGE and will be recorded only for samples which values has changed in time since the beginning of the page. If sampled data for given sensor never change then that data is only stored as absolute value at the beginning of the page only. Size of each page is fixed  and must be predefined i.e. 256 bytes.

We define following types for measured data and their default (max) lengths. When type is logged with shorter then default length in the buffer it is assumed to be delta from previously measured value.

We will use TLV (Type Length Value) format to record each data point (including time) with TL stored in one byte (upper 4 bits for TAG and lower 4 for Length) -  only 16 types can be defined with each having 16 bytes in size. Smallest sample value will require 2 bytes in the buffer: 1 byte for Type and Length, and 1 byte for value while biggest sample can take up as much as 17 bytes (1 TL + 16 bytes for value).
---
**Example of sampled types with their Tag value defined as TYPE_ENUM and typical lengths**:
'
TYPE_ENUM = {
TIME = 0		// 1 to 4 bytes
TEMP,			// 1 to 2 bytes
HUMIDITY,		// 1 byte
LIGHT,			// 1 to 2 bytes
SHOCK,		// 1 bytes
INCLINE,		// 3  to 6 bytes
LONGITUDE,		// 1 to 4 bytes
LATITUDE,		// 1 to 4 bytes
STRING, 		// 1 to 16 bytes
}  //  Note: MAX 16 TYPES can be predefined
'

##Event/Logging Buffer

Buffer is implemented as linear array containing many pages of data with each page starting with absolute values of all sampled data. Each Page is exactly PAGE_BYTE_LENGTH long.
Each Page must start with TWO absolute TIME types (Time of first sample and time of last sample), in order to define timeframe of sampled data in the page. Thus, minimum size page would contain full set of measured types with one beginning and one ending timestamp.
In other words, each Page is a minimum size data packet of parseable samples without any external dependencies on other pages with a clear time frame. Start of any page can be easily located in the buffer by jumping from the start of a buffer by PAGE_BYTE_LENGHT bytes. Time frame of each page can be easily figured out by reading first 10 (or less) bytes of data.
---
**Note**: The TIME type must start with full 4 byte linux timestamp but could end with just 2 byte long time stamp if it encloses values within 65536 second interval. Each page contains any combination of types.

'BUFFER: PAGE(256 bytes),PAGE(256bytes), PAGE(256 bytes)…..'

---
Example PAGE (showing each byte separated by comma) shows start Time in full unix timestamp followed by delta End time of Page, followed by full value of TEMP and LIGHT followed by -12 degree change in temperature after 33 seconds and followed by ending TIME stamp of 90 seconds where LIGHT Changed by 55 :
'
TIME<<4 | 04, 12,34,56,78, TIME<<4 | 01, 200,TEMP<<4 | 02,44,44,LIGHT<<4 | 02,
55,55,TIME<<4 | 1,33, TEMP<<4 | 01,-12,TIME<<4 | 1, 90, LIGHT<<4 | 01, 55
'
