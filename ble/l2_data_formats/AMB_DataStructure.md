
## L1 Device 16B Data Structure

Data composition of 16 Bytes as follows:

1. 4B Unix Timestamp
1. 1B Temperature
1. 1B Humidity
1. 2B Optical Power
1. 1B Acceleration  LSB
1. 3B Acceleration  \[x,y,z]
1. 3B Averaged Acceleration  \[x,y,z]
1. 1B Averaged Acceleration  LSB

#### Path :
```Ambrosus-L2-GW/app/src/main/java/com/crayonic/ambrosus/gateway/model```
#### Class :
```AMB_DataStructure```

### Usage

#### From ByteArray
```
val structure = AMB_DataStructure(bytes)
```

#### From ByteArray To List
```
var receivedData : ByteArray
...
// somehow received raw data from L1 device
...
receivedData.toListOfAMB_DataStructure()
```
---
## Decoding the values

### Unix Timestamp
4B : little endian 32 bit unsigned integer
#### Unit
s - seconds from epoch
#### Conversion from Little Endian
```
fun ByteArray.fromLittleEndian2Long(): Long {
    if (size < 4)
        return 0


    return (this[3].toInt().and(0xff)
            ).toLong().shl(24) or (this[2].toInt().and(0xff)
            ).toLong().shl(16) or (this[1].toInt().and(0xff)
            ).toLong().shl(8) or (this[0].toInt().and(0xff)).toLong()
}
```
---
### Temperature
1B : 8 bit unsigned integer
#### Unit
°C - Celsius
#### Conversion to Double

```
    fun temperature(byte: Byte):Double{
        return (HexUtil.signedCharToInt(byte) + 32).toDouble() / 2.0
    }
```
---
### Humidity
1B : 8 bit unsigned integer
#### Unit
% - 0-100 relative humidity
#### Conversion to Integer
```
fun Byte.toIntFromUint8(): Int {
    return this.toInt() and 0xFF
}
```
---
### Optical Power
1B : Little Endian 16 bit unsigned integer
#### Unit
nW/cm 2
#### Conversion to Double
```
    /**
     * Take 16 bits of unsigned int 16 ordered in Little Endian
     * This register R contains the result of the most recent light-to-digital conversion.
     * This 16-bit register has two fields: a 4-bit exponent and a 12-bit mantissa.
     */
    fun opticalPower(value: ByteArray): Double {
        if (value.size >= 2) {
            // convert little endian
            val input =
                     value[1].toInt().and(0xff).shl(8)
                    .or(  value[0].toInt().and(0xff ) )
//            Optical_Power = R[11:0] × LSB_Size
            val exponent = input.opticalPowerExponentRegister()
            val readValues = input.opticalPowerReadRegister()

            return opticalPowerEquation(exponent,readValues)
        }
        return 0.0
    }

    fun opticalPowerEquation ( expReg: Int, readReg: Int ): Double{
        return opticalPower_LSB_Size(expReg) * readReg
    }

    fun Int.opticalPowerExponentRegister() : Int{
        return this.and(0xf000).ushr(12)
    }

    fun Int.opticalPowerReadRegister() : Int{
        return this.and(0x0fff)
    }

    fun opticalPower_LSB_Size(exponent: Int): Double {
        return if(exponent in 0..11){
            1.2 * 2.0.pow(exponent)
        }else{
            1.2
        }
    }

```
---
### Acceleration
1B : 8 bit unsigned integer for LSB of all three axes (using 6 bits only)
3B : 3 axes \[x,y,z], each 8 bit unsigned integer
#### Unit
g - Gravitational acceleration
#### Conversion to list of Doubles
```
    fun decodeAcc_12B(_3B_msb: ByteArray, _1B_lsb: Byte): List<Double>{
        val x = (_1B_lsb.toIntFromUint8().and(48).ushr(4)
                .or(
                        _3B_msb[0].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_12B

        val y = (_1B_lsb.toIntFromUint8().and(12).ushr(2)
                .or(
                        _3B_msb[1].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_12B

        val z = (_1B_lsb.toIntFromUint8().and(3).ushr(0)
                .or(
                        _3B_msb[2].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_12B

        return listOf(x, y, z)
    }
```
---
### Averaged Acceleration
3B : 3 axes \[x,y,z], each 8 bit unsigned integer
1B : 8 bit unsigned integer
#### Unit
g - Gravitational acceleration
#### Conversion to list of Doubles
```
    fun decodeAcc_12B(_3B_msb: ByteArray, _1B_lsb: Byte): List<Double>{
        val x = (_1B_lsb.toIntFromUint8().and(48).ushr(4)
                .or(
                        _3B_msb[0].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_12B

        val y = (_1B_lsb.toIntFromUint8().and(12).ushr(2)
                .or(
                        _3B_msb[1].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_12B

        val z = (_1B_lsb.toIntFromUint8().and(3).ushr(0)
                .or(
                        _3B_msb[2].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_12B

        return listOf(x, y, z)
    }
```
---
