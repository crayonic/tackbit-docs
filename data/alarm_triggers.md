# Environmental Excursion Zones  - Specification


The transported product quality largely depends upon the storage environmental conditions. Natural reasons or human negligence could create uncalled-for situation causing excursions into unwanted environmental conditions such as temperature, humidity, light, vibrations, etc. and unwanted combination of those ie. condensation of high humidity in lower temperatures. The most important environmental parameter having significant potential to impact quality of cold chain products is temperature. If the temperature excursions are not handled systematically, there shall be an adverse impact on product quality.

Environmental excursion zones (EEZ) supported by Trackbit device are always combination of all measured variables and time. Meaning, that if all environmental conditions defined by MIN and MAX values of zone are met for predefined amount of time the zone alarm will be raised! These condition are thus evaluated all at once in AND style condition statement. Any predefined zone in Trackbit device supports any combination of environmental variables which in turn mean if certain environmental variable is not important for the EEZ it needs to have its MIN and MAX values set to extreme values i.e. if excursion into temperature only zone should be triggered then humidity for that zone must be defined as min=0, max=100 and similar applies to other ignored variables.
From this perspective, it is useful to define at least one default zone which can define ideal environmental values for all variables where product would either never expire or expire because it has some maximum shelf life for the ideal conditions. Thus, we propose to always define this default zone (Zone 0). Zone 0 has ideal storage conditions set or if none matter then they should have extreme MIN/MAX values.

## TIME
Besides defining environmental values for zones for later compliance reporting, each zone also contains MIN and MAX time values which are used by Trackbit device to trigger and snooze excursion zone alarms!

Trackbit device will trigger alarm for each zone independently and try to broadcast this information ASAP to remote service so action can be taken before product damage. The broadcasting itself depends on Trackbit device implementation of radio stack i.e. BTLE, LORA, GSM.

Current implementation enables for MIN Time and MAX Time setting per zone where:
T(min) => Time (in seconds) when zone alarm will triggered (broadcasting started) after T(min) seconds of excursion into given EEZ.
As soon as alarm has been received by remote service the alarm trigger will be snoozed for T(max) seconds.
In another words, the remote service should not receive same alarm from one Trackbit in less then T(max) .
T(max) => Time (in seconds) to re-trigger  alarm for the zone if still in the zone for longer then T(min) time.

## SHOCK
Shock is environmental variable that can be made part of any EEZ and is checked first (max priority in reporting) so it is not wise to combine it with other environmental variables. 
Additionally, shock triggers alarm immediately (no T(min) period required) as should be obvious. Only MAX Shock is used in zone definitions and MIN Shock value is ignored or set to same as MAX value.

## EEZ definitions

Zones are defined using JSON object on the side of API and using binary C structure on the side of L1 device. 
To be compatible across API and devices we use integers even where floating point numbers could be used i.e. temperature, shock. This means that in API and C structure some variables are multiplied by 10 (single digit precision) and 100 (double digit precision).  Temperature is currently recorded and defined in single digit precision thus is multiplied by 10 in zone definition and so are the output data from Trackbit device. Shock has double digit precision and needs to be multiplies by 100.
Currently the only way to set zones is during Trackbit provisioning with mobile device and Trackbit application.

**Each zone has set of Maximum and Minimum values with their possible extremes listed below.** 


**MIN**
  

      time = 4294967295   seconds
      temperature = -3276.7  C
      humidity = 0  %
      optical_power = 0  nW/Cm2
      shock = 0.0  G
      incline_x = -180  Deg
      incline_y = -180  Deg
      incline_z = -180  Deg


**MAX**  
  

      time = 4294967295 seconds
      temperature = 3276.7  C
      humidity = 100  %
      optical_power = 2147483647 nW/Cm2
      shock = 100.00 G
      incline_x = 180  Deg
      incline_y = 180  Deg
      incline_z = 180 Deg

## UI
Below are the screen shots on how the UI is implemented on mobile side to setup the zones.

**NOTE**: UI currently does not check if zone makes sense or not thus it is possible to create different zones that overlap each other, thus some end user knowledge and checks are required otherwise unpredictable results are possible.


# FORMATS APENDIX


## JSON format
Zones are defined in sequence using JSON array structure with each zone having one object with 2 sub objects (max and min).


**Sample zone definition format for API:**

    {
       "type":"crayonic.amb.l1.zones.v1_0_0",  // type for the DB search
       "zones":[
          {
             "max":{
                "humidity":100,
                "incline_x":180,
                "incline_y":180,
                "incline_z":180,
                "optical_power":2147483647,
                "shock":10000,
                "temperature":325,
                "time":4294967295
             },
             "min":{
                "humidity":0,
                "incline_x":-180,
                "incline_y":-180,
                "incline_z":-180,
                "optical_power":0,
                "shock":0,
                "temperature":-480,
                "time":4294967295
             }
          },
          {
             "max":{
                "humidity":100,
                "incline_x":180,
                "incline_y":180,
                "incline_z":180,
                "optical_power":2147483647,
                "shock":10000,
                "temperature":795,
                "time":4294967295
             },
             "min":{
                "humidity":0,
                "incline_x":-180,
                "incline_y":-180,
                "incline_z":-180,
                "optical_power":0,
                "shock":0,
                "temperature":325,
                "time":1
             }
          },
          {
             "max":{
                "humidity":95,
                "incline_x":180,
                "incline_y":180,
                "incline_z":180,
                "optical_power":2147483647,
                "shock":10000,
                "temperature":32767,
                "time":4294967295
             },
             "min":{
                "humidity":63,
                "incline_x":-180,
                "incline_y":-180,
                "incline_z":-180,
                "optical_power":0,
                "shock":0,
                "temperature":-32767,
                "time":1
             }
          },
          {
             "max":{
                "humidity":100,
                "incline_x":180,
                "incline_y":180,
                "incline_z":180,
                "optical_power":10000,
                "shock":10000,
                "temperature":32767,
                "time":4294967295
             },
             "min":{
                "humidity":0,
                "incline_x":-180,
                "incline_y":-180,
                "incline_z":-180,
                "optical_power":0,
                "shock":0,
                "temperature":-32767,
                "time":1
             }
          },
          {
             "max":{
                "humidity":100,
                "incline_x":180,
                "incline_y":180,
                "incline_z":180,
                "optical_power":2147483647,
                "shock":672,
                "temperature":32767,
                "time":4294967295
             },
             "min":{
                "humidity":0,
                "incline_x":-180,
                "incline_y":-180,
                "incline_z":-180,
                "optical_power":0,
                "shock":672,
                "temperature":-32767,
                "time":2147483647
             }
          }
       ]
    }

## **Binary zone definition format:**

In Trackbit device the binary C structure format is used in native little endian with one zone set per one APDU command - first MIN values then MAX values immediately follow.

    {
        uint32_t time; 
        int16_t  temp; 
        uint16_t humid;
        uint32_t lux;   
        uint16_t shock;
        int16_t  incline[3];
    }min;
    {
        uint32_t time; 
        int16_t  temp; 
        uint16_t humid;
        uint32_t lux;   
        uint16_t shock;
        int16_t  incline[3];
    }max;
