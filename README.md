# Trackbit Documentation

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Trackbit Documentation](#amb-l1-documentation)
	- [Lifecycle](#lifecycle)
	- [Usage with Apps](#usage-with-apps)
		- [Provisioning](#provisioning)
		- [Administration / Testing](#administration-testing)
	- [Communication](#communication)
		- [NFC](#nfc)
		- [Bluetooth / BLE](#bluetooth-ble)
			- [BLE Characteristics](#ble-characteristics)
		- [APDU Transmission](#apdu-transmission)
			- [Format of APDU command](#format-of-apdu-command)
		- [Data Formats](#data-formats)
		- [Advanced Communication Protocols](#advanced-communication-protocols)
	- [SDK Integration](#sdk-integration)
	- [Other Documents links](#other-documents-links)

<!-- /TOC -->


## Lifecycle

Trackbit Device has multiple states of operation during its life time. Depending on which state it is in it will have different behaviour and security needs.

[](./img/l1_states.png)

1. Manufacturing State

2. Unprovisioned State

3. Provisioned State

4. End-of-life state

* Firmware Update State

More details on Trackbit states and behavior can be found in [Security model](./pdf/Security_model.pdf).

## Usage with Apps

### Provisioning

The AMB Provisioning android application serves currently 2 core purposes:

1. Easy device Initialization
2. Easy reading and posting of AMB Event

User guide : [AMB Provisioning](./pdf/AMB_Provisioning.pdf)

### Administration / Testing

The AMB Admin android application serves currently 3 core purposes:

1. Easy testing and DFU updating of L1 device
2. Selection and provisioning of L1 with parameter settings
3. SW L2 gateway emulation

User guide : [Trackbit Android app user guide](./pdf/AMBL1_Android_app_user_guide.pdf)


## Data Formats

1. [Data Sampling v2 - Delta logging of sensor measurements](./data/sensor_sampling_v2.md)
2. [Event Creation](./data/event_creation.md)
3. [Environmental Excursion Zones](./data/alarm_triggers.md)


## Communication

### NFC

Trackbit device can be waken up from deep sleep with NFC enabled device. Device advertises following NFC Tag/s :

| Tag  | Tag Type | Example                                                  | Detail                                                        |
|---------|----------|----------------------------------------------------------|---------------------------------------------------------------|
| BLE Mac | TEXT     | http://ambrosus.crayonic.com/device/l1/AB-CD-EF-12-34-56 | Used for connecting to device after waking up from deep sleep |
| ...     | ...      | ...                                                      | ...                                                           |

### Bluetooth / BLE

Trackbit starts advertising on BLE freq. after waken up by NFC or sensoric alarm triggered from deep sleep. Device supports one BLE connect/disconnect after which it goes back to deep sleep. While connected to the device one can read and write BLE characteristics compliant with BLE GATT.

Trackbit acts as BLE GATT Server using following BLE characteristics :

#### BLE Characteristics

|   | Name                   | Type       | Characteristic UUID                                                                                                 | GATT Operation | Detail                                                                                                          |
|---|------------------------|------------|---------------------------------------------------------------------------------------------------------------------|----------------|-----------------------------------------------------------------------------------------------------------------|
| * | Generic Service        | Service    | 0x1800                                                                                                              |                |                                                                                                                 |
| - | Device name            | Char.      | 0x1801                                                                                                              | Read           |                                                                                                                 |
|   |                        |            |                                                                                                                     |                |                                                                                                                 |
| * | Crayonic RX/TX         | Service    | 9eb70001-8c04-4c98-ae44-2ca32bfa549a                                                                                |                | Binary APDU Transmission service.                                                                               |
| - | TX Channel             | Char.      | 9eb70002-8c04-4c98-ae44-2ca32bfa549a                                                                                | Write          | Send APDU Bytes to device. Turn on notifications on CCCD descriptor to receive Bytes from device on RX Channel. |
| - | RX Channel             | Char.      | 9eb70003-8c04-4c98-ae44-2ca32bfa549a                                                                                | Notify         | Receive Bytes from device.                                                                                      |
|   | CCCD                   | Descriptor | 0x2902                                                                                                              | Set Notif.     | Client Characteristic Configuration                                                                             |
|   |                        |            |                                                                                                                     |                |                                                                                                                 |
| * | Battery Service        | Service    | 0x180F                                                                                                              |                |                                                                                                                 |
| - | Battery Level          | Char.      | 0x2A24                                                                                                              | Read, Notify   | Turn on notifications on CCCD descriptor to receive battery level updates.                                      |
|   | CCCD                   | Descriptor | 0x2902                                                                                                              | Set Notif.     | Client Characteristic Configuration                                                                             |
|   |                        |            |                                                                                                                     |                |                                                                                                                 |
| * | Device Information     | Service    | 0x180A                                                                                                              |                |                                                                                                                 |
| - | Model Number String    | Char.      | 0x2A24                                                                                                              | Read           | Example: "L1"                                                                                                   |
| - | Firmware Number String | Char.      | 0x2A26                                                                                                              | Read           | Example: "1.8"                                                                                                  |
|   |                        |            |                                                                                                                     |                |                                                                                                                 |
| * | AMB Device State       | Service    | 9eb7fd00-8c04-4c98-ae44-2ca32bfa549a                                                                                |                | Contains information of current device setup and real time values from sensors.                                 |
| - | AMB Account            | Char.      | 9eb7d001-8c04-4c98-ae44-2ca32bfa549a                                                                                | Read           | ETH address of L1 account. (String format) Example: "24E2CEB7d6E96CeAaEB5e3c6ec413fC433251d40"                  |
|   | Char. User Descriptor  | Descriptor | 0x2901                                                                                                              |                | Field name. Value: "ETH address"                                                                                |
| - | AMB Asset ID           | Char.      | 9eb7d002-8c04-4c98-ae44-2ca32bfa549a                                                                                | Read           | Asset ID affiliated by L1. (Binary format)                                                                      |
|   | Char. User Descriptor  | Descriptor | 0x2901                                                                                                              |                | Field name. Value: "Asset ID"                                                                                   |
| - | Temperature Sensor     | Char.      | 9eb7d010-8c04-4c98-ae44-2ca32bfa549a                                                                                | Read           |                                                                                                                 |
|   | Char. User Descriptor  | Descriptor | 0x2901                                                                                                              |                | Field name. Value: "T[Celsius x 10]"                                                                            |
| - | Humidity Sensor        | Char.      | 9eb7d011-8c04-4c98-ae44-2ca32bfa549a                                                                                | Read           | Measuring relative humidity                                                                                     |
|   | Char. User Descriptor  | Descriptor | 0x2901                                                                                                              |                | Field name. Value: "HR[%]"                                                                                      |
| - | Light Sensor           | Char.      | 9eb7d012-8c04-4c98-ae44-2ca32bfa549a                                                                                | Read           | Measuring optical power                                                                                         |
|   | Char. User Descriptor  | Descriptor | 0x2901                                                                                                              |                | Field name. Value: "L[nW/cm2]"                                                                             |
| - | Shock Sensor           | Char.      | 9eb7d013-8c04-4c98-ae44-2ca32bfa549a                                                                                | Read           | Measuring magnitude of impact acceleration                                                                      |
|   | Char. User Descriptor  | Descriptor | 0x2901                                                                                                              |                | Field name. Value: "G[g x 100]"                                                                                 |
|   |                        |            |                                                                                                                     |                |                                                                                                                 |
| * | Firmware Update (DFU)  | Service    | see [Nordic DFU protocol](https://www.nordicsemi.com/DocLib/Content/SDK_Doc/nRF5_SDK/v15-2-0/lib_dfu_transport_ble) |                | While in Firmware Update state, Trackbit can receive FW package over DFU Service (maintained in nRF SDK).         |


### APDU Transmission

Trackbit is programmed to accept only well formatted byte streams in compliance with APDU Extended standard as in ISO7816.

Trackbit implements protocol logic based on two APDU records READ_RECORD and WRITE_RECORD.

#### Format of APDU command
Core parts of each APDU command are Header (mandatory first 7B of command), Data (variable length payload), Status Word (mandatory 2B reponse). Following table shows bytes ordering and fields in standard APDU command (up to N bytes) supported by Trackbit device :

| Byte # 	| Field       	| 0x Value    	| Byte Length 	| Description                          	|
|--------	|-------------	|-------------	|-------------	|--------------------------------------------------------------------------------------------------------------------------------------------	|
| 1      	| INS         	| FF          	| 1           	| Channel specific value                                                                                                                     	|
| 2      	| CLA         	| B2/D2       	| 1           	| Read or write record                                                                                                                       	|
| 3,4    	| P1,P2       	| eg. 0100    	| 2           	| Defines command type. See [APDU Commands Table](./apdu_commands.md) for all APDU commands.                                                                                                                      	|
| 5      	| L0          	| 00          	| 1           	| Always 0x00 in APDU Extended                                                                                                               	|
| 6,7    	| L1,L2       	| 0000 - FFFF 	| 2           	| Defines length of Data field. Range from 0 to 65535                                                                                        	|
| ...    	| Data        	| ...         	| in L1,L2    	| Command payload. Field with variable length. Omitted in case of any error.                                                                 	|
| N-1, N  	| Status Word 	| eg. 9000    	| 2           	| Response that ends every APDU command. Either success or error. For all supported status words see [Status Words Table](./status_words.md) 	|


### Advanced Communication Protocols

1. [Two way APDU communication](./protocols/two_way_apdu.md)
2. [Secure Protocols](./protocols/security_protocols.md)
3. [Event Proxy Relay](./protocols/event_proxy.md)

## SDK Integration

... TBD

## Other Documents links

1. [Security model](./pdf/Security_model.pdf)
1. [HW Considerations](./pdf/HW_design_considerations.pdf)
2. [Trackbit protocols and communication](./pdf/AMBL1_protocols_and_communication.pdf)
3. [Trackbit Android app user guide](./pdf/AMBL1_Android_app_user_guide.pdf)
4. [AMB Provisioning](./pdf/AMB_Provisioning.pdf)
