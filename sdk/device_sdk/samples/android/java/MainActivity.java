package com.crayonic.device_sdk_android.sample

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.crayonic.device_sdk_android.*;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import space.lupu.koex.strings.StringExtKt;

/**
 * Pseudo implementation of the Devices SDK for Android
 */
public class MainActivity extends AppCompatActivity
        implements CrayonicServiceListener, CrayonicDeviceListener {

    /// service calls

    CrayonicService crayonicService = CrayonicService.Companion.getRunningInstance(this);

    @Override
    protected void onResume() {
        if(crayonicService == null)
            crayonicService = CrayonicService.Companion.getRunningInstance(this);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if(crayonicService == null)
            crayonicService.unbindService(this);
        super.onDestroy();
    }

    /// list devices

    Set<CrayonicDevice> foundDevices = new HashSet<CrayonicDevice>();


    public void listDevices(){
        DeviceFilter filter = new DeviceFilter(); // matches any reachable device
        crayonicService.listDevices(filter, this);
    }

    @Override
    public void onDevicesFound(List<CrayonicDevice> devices) {
        foundDevices.addAll(devices); // add devices to your collection
    }

    /// select and connect devices

    CrayonicDevice selectedDevice = null;

    private void crayonicDeviceSelectedByUser( CrayonicDevice device ){
        if(selectedDevice != null) selectedDevice.disconnect();

        selectedDevice = device;
        // connect and wait until callback gets here
        device.connect(this);
    }

    /// start communication

    @Override
    public void onConnect(CrayonicDevice device) {
        Toast.makeText(this,"Device connected", Toast.LENGTH_SHORT).show();
        startRequestingData(device);
    }

    private void startRequestingData(CrayonicDevice device) {
        device.readDeviceName(this);
        device.readBatteryLevel(this);
    }

    @Override
    public void onDisconnect(CrayonicDevice device) {
        Toast.makeText(this,"Device disconnected", Toast.LENGTH_SHORT).show();
    }

    /// communication handling

    Map<CrayonicDevice, String> knownPinCodes =  getPinCodeDatabase();

    @Override
    public void onResult(CrayonicDevice device, RequestCode requestKey, Map<String, Object> values) {
        // device is null safe
        if(device != selectedDevice) return;

        if(requestKey == RequestCode.readBatteryLevel)
            updateUIreadBatteryLevel( (String) values.get("readableValue"));
        if(requestKey == RequestCode.readDeviceName)
            updateUIreadDeviceName( (String) values.get("readableValue"));
        else
        // disconnect when task response was received
            device.disconnect(this); 


        // for some operation on device pin code is necessary
        String adminPinCode = knownPinCodes.get(device);

        // filter different type of devices
        //    and run device specific task 
        if(device.getDeviceTags().contains("L1")){
            // read alarms
            ((CrayonicL1) device).runTaskReadEvents(this);
        }else if(device.getDeviceTags().contains("Pen")){
            // read the signing data
            if(adminPinCode != null)
                ((CrayonicPen) device).runTaskReadData(adminPinCode,this);
        }else{
            // try to update unknown device model if we have the PIN
            if(adminPinCode != null)
                device.runTaskUpdateDevice(adminPinCode, this);
        }
    }

    @Override
    public void onError(CrayonicDevice device, CrayonicRequestCode reqKey, CrayonicDeviceError error) {
        // ignore all but current device
        if(device != selectedDevice) return;

        // error handling of callback error instance
        if(error.getErrorType() == CrayonicDeviceErrorType.NoError){
            // all is good
        }else{
            logError(device, error);
        }

        // error handling of device error instance
        CrayonicDeviceError errorFlag = device.getErrorFlag();  
        switch(errorFlag.getErrorType()){
            case NoError:
                // all is good
                break;
            case DeviceOutOfReachError:
                showUiRationaleDeviceOutOfReach(device);
                logError(device, errorFlag);
                break;
            default:
                logError(device, errorFlag);
                break;
        }
    }


    /// standard android activity flow

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView tw = findViewById(R.id.textView);
        Random r = new Random(System.currentTimeMillis());
        tw.setText(StringExtKt.nextStringInt(kotlin.random.Random.class.cast(r),9, '0'));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


    }

    /// custom helper methods

    private void updateUIreadDeviceName(String readableValue) {
        // TODO: implement UI interaction
    }

    private void updateUIreadBatteryLevel(String readableValue) {
        // TODO: implement UI interaction
    }

    private void logError(CrayonicDevice device, CrayonicDeviceError error) {
        Log.e("MainActivity", "logError on device ["+device.toString()+"]: "+error.getMessage(), error);
    }

    private Map<CrayonicDevice, String> getPinCodeDatabase() {
        // TODO : implement call for keystore
        return null;
    }
}