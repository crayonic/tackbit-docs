# Scripting Commands
One of the capabilities of Android L2 GW is running well formatted JSON script file. Ths functionality can be found in Command Center under "Run Script" Command.

## Usage 
1. Open Command Center
1. "Select Device". L1 should be powered on, in proximity of L2 device and ready to connect
1. Select "Run script" from Commands
1. Select script with "SELECT FILE" option of "Run Script" command
** only .json files are supported
1. Execute the command with "Send Command"

![Steps to take on phone](/home/lukassos/Documents/projects/Crayonic/Ambrosus/Ambrosus-L2-GW/docs/scripting/cc_usage_w500.png) 


---

## Inside logic and script formatting
Script should be formatted in JSON syntax. 

   * Data is in key/value pairs
   * Data is separated by commas
   * Curly braces hold objects
   * Square brackets hold arrays



#### Execution queue
Each script executed runs ***Commands*** ordered in json array **exec_que**.  Description and setup for these ***Commands*** are unordered inside of json array **commands**. Commands are refferenced base on unique string identifier ***exec_id***. 

#### Expected values
After command is executed normally status word **sw** of apdu response is compared (by default expecting **9000**). If **expected** > **data** is specified each value is compared to **output**. **expected** > **data** is an array of JSON objects containing **value**, **format**, **comparision operator**, **value_exec_id**. Hint to set the compare operator correctly : ```Comparing EXPECTED to OUTPUT```. Meaning with **comparison** = "<" comparing values is true iff ```EXPECTED value < OUTPUT value```

#### Stop conditions
Commands execution is interrupted if any of **stop_condition**s is met. It is optional, but recommended to use all stop conditions. Commands can be executed multiple times, max up to **repeat_count**. Each execution has optional **timeout** set in milliseconds and option to stop **on_exception** if any exception ocurs. 

#### Supported command types
* APDU Commands - executes one of Apdu Ble Models
	* READ - Read record APDU commands
	* SET - Admin write record APDU commands
* WAIT - dalay for given time (in seconds)
* REPEAT - jump in the **exec_que** to a specific **exec_id**
* PREFS Commands - Setup of L2 over script
* VALID Commands - Validate input values
* EXTRACT Commands - Extract value from complex output format (e.g. event_id from JSON AMB event )
* HTTP Commands - Send HTTP request over L2 

#### Supported commands

|"cmd"|description|
|---|---|
| "READ_BUFFER" | Read all buffered data |
| "READ_SAMPLING" | Read sampling rate |
| "SET_SAMPLING" | Set sampling rate |
| "READ_CLOCK" | Read clock of L1 device in seconds from epoch |
| "SET_CLOCK" | Set clock of L1 device in seconds from epoch |
| "READ_LOW_WATER" | Read low watermark |
| "SET_LOW_WATER" | Set low watermark |
| "READ_HIGH_WATER" | Read high watermark |
| "SET_HIGH_WATER" | Set high watermark |
| "READ_PUB_KEY" | Read public key that was set during L1 provisioning |
| "SET_GEN_KEYS" | Generate keys – can be used during L1 provisioning |
| "READ_ETH_ADD" | Read ETH (checksum) address of L1 device |
| "READ_ACCOUNT" | Read ETH (checksum) address of L1 device (eq to READ_ETH_ADD) |
| "SET_ETH_SEED" | Generate keys with specific seed |
| "READ_ASSET" | Read Asset ID  |
| "SET_ASSET" | Set Asset ID |
| "SET_EVENTS_START" | Start event process – locks buffer to prevent bytes overwrite and generates signed AMB event json from gathered data |
| "READ_EVENT_LEN" | Read length of the generated event from “SET_EVENT_START”, to be used as expected length |
| "READ_EVENT_DATA" | Read event data – data bytes are of signed json event in specific format (bytes to be parsed, converted and corrected before usage), uses expected length from "READ_EVENT_LEN" |
| "SET_EVENTS_FINISH" | Finish the event process – unlock buffer, remove gathered data if event_id was provided as event process crc, leave them intact otherwise |
| "SET_MODEM_SETUP" | Sets modem setup string to L1 extended device. String contains 4 parts separated with \|. Example : 600\|apn_name\|https://gateway-test.ambrosus.com/assets/[ASSET]/events\|https://gateway-test.crayonic.com/log |
| "VALID_L1_EVENT" | Validate bytes received from "READ_EVENT_DATA" – does data parsing, check of json fields and crypto signature verification |
| "VALID_ASSET" | Validate bytes input  received from "READ_ASSET"– does dlength check |
| "VALID_ETH_ADD" | Validate bytes input  received from "READ_ETH_ADD"– does dlength check |
| "VALID_TOKEN" | Validate bytes of AMB Token – does length check |
| "VALID_SECRET" | Validate bytes input of Private key used in "PREFS_SECRET" or “SET_ETH_SEED” – does length check |
| "VALID_PUB_KEY" | Validate bytes input of public key received from “READ_PUB_KEY”– does dlength check |
| "PREFS_ASSET" | Setup L2 preference : Asset ID |
| "PREFS_ACCOUNT" | Setup L2 preference : Account ID / ETH address |
| "PREFS_TOKEN" | Setup L2 preference : AMB Token |
| "PREFS_SECRET" | Setup L2 preference : Private Key, derived Public Key and derived ETH address |
| "PREFS_PUB_KEY" | Setup L2 preference : Public Key |
| "POST_ACCOUNT" | Send HTTP POST request to AMB create account |
| "POST_EVENT" | Send HTTP POST request to AMB create event – sends event data to blockchain |

#### Supported Command default input/output

| "cmd"| input value |  input format | output value |  output format | 
|---| --- | --- | ---| ---|
|"READ_BUFFER"|null|"no_data"|ReadData_ApduBleModel.DEFAULT_OUTPUT_VALUE|"bytes"|
|"READ_SAMPLING"|null|"no_data"|ReadSampling_ApduBleModel.DEFAULT_SET_VALUE|"int"|
|"SET_SAMPLING"|SetSampling_ApduBleModel.DEFAULT_SET_VALUE|"int"|null|"no_data"|
|"READ_CLOCK"|null|"no_data"|ReadClock_ApduBleModel.DEFAULT_SET_VALUE|FORMAT_LONG|
|"SET_CLOCK"|SetTime_ApduBleModel.DEFAULT_SET_VALUE|"long"|null|"no_data"|
|"READ_LOW_WATER"|null|"no_data"|ReadLowWatermark_ApduBleModel.DEFAULT_SET_VALUE|"int"|
|"SET_LOW_WATER"|SetLowWaterMark_ApduBleModel.DEFAULT_SET_VALUE|"int"|null|"no_data"|
|"READ_HIGH_WATER"|null|"no_data"|ReadHighWatermark_ApduBleModel.DEFAULT_SET_VALUE|"int"|
|"SET_HIGH_WATER"|SetHighWaterMark_ApduBleModel.DEFAULT_SET_VALUE|"int"|null|"no_data"|
|"READ_PUB_KEY"|null|"no_data"|ReadPublicKey_ApduBleModel.DEFAULT_OUTPUT_VALUE|"bytes"|
|"SET_GEN_KEYS"|SetGenerateKeyPair_ApduBleModel.DEFAULT_SET_VALUE|"int"|null|"no_data"|
|"READ_ETH_ADD"|null|"no_data"|ReadEthAddress_ApduBleModel.DEFAULT_OUTPUT_VALUE|"bytes"|
|"READ_ACCOUNT"|null|"no_data"|ReadEthAddress_ApduBleModel.DEFAULT_OUTPUT_VALUE|"bytes"|
|"SET_ETH_SEED"|SetEthSeed_ApduBleModel.DEFAULT_SET_VALUE|"bytes"|null|"no_data"|
|"READ_ASSET"|null|"no_data"|ReadAssetId_ApduBleModel.DEFAULT_OUTPUT_VALUE|"bytes"|
|"SET_ASSET"|SetAssetId_ApduBleModel.DEFAULT_SET_VALUE|"bytes"|null|"no_data"|
|"SET_EVENTS_START"|SetEventStartReading_ApduBleModel.DEFAULT_SET_VALUE|"int"|null|"no_data"|
|"READ_EVENT_LEN"|null|"no_data"|ReadEventLength_ApduBleModel.DEFAULT_SET_VALUE|"int"|
|"READ_EVENT_DATA"|null|"int"|ReadEventData_ApduBleModel.DEFAULT_OUTPUT_VALUE|"bytes"|
|"SET_EVENTS_FINISH"|SetEventFinishReading_ApduBleModel.DEFAULT_SET_VALUE|"bytes"|null|"no_data"|
|"SET_EVENTS_FINISH"|SetModemSetup_ApduBleModel.DEFAULT_SET_VALUE|"string"|null|"no_data"|
|"VALID_L1_EVENT"|0 bytes|"bytes"|SetEventStartReading_ApduBleModel.DEFAULT_SET_VALUE|"bytes"|
|"VALID_ASSET"|bytes of L2 settings value|"bytes"|"invalid"|"string"|
|"VALID_ETH_ADD"|bytes of L2 settings value|"bytes"|"invalid"|"string"|
|"VALID_TOKEN"|bytes of L2 settings value|"bytes"|"invalid"|"string"|
|"VALID_SECRET"|bytes of L2 settings value|"bytes"|"invalid"|"string"|
|"VALID_PUB_KEY"|bytes of L2 settings value|"bytes"|"invalid"|"string"|
|"PREFS_ASSET"|bytes of L2 settings value|"bytes"|bytes of L2 settings value|"bytes"|
|"PREFS_ACCOUNT"|bytes of L2 settings value|"bytes"|bytes of L2 settings value|"bytes"|
|"PREFS_TOKEN"|bytes of L2 settings value|"bytes"|bytes of L2 settings value|"bytes"|
|"PREFS_SECRET"|bytes of L2 settings value|"bytes"|bytes of L2 settings value|"bytes"|
|"PREFS_PUB_KEY"|bytes of L2 settings value|"bytes"|bytes of L2 settings value|"bytes"|
|"POST_ACCOUNT"|0 bytes|"bytes"|418|"int"|
|"POST_EVENT"|0 bytes|"bytes"|418|"int"|

---

## Structure
In following section data are marked with : 

| symbol | detail | 
| --- | --- |
| **(m)**  | mandatory data |
| **(o)**  | optional data |
| **(mt)**  | mandatory for specific type only |
|  |  |
| **[ao]** |  JSON Array of objects |
| **[o]** |  JSON Object |
| **[av]** | JSON Array of values |
| **[v]** |  value |
| **[t]** |  type |
|  |  |
| **{AR}** | APDU Read command |
| **{AS}** | APDU Set command | 
| **{W}** | WAIT command | 
| **{R}**  | REPEAT command | 
| **{V}**  | VALID command | 
| **{P}**  | PREFS command | 
| **{H}**  | HTTP command | 


```
* "commands" (m) [ao]
	* "cmd" - (m) - [v][t] string - Command type
	* "exec_id" - (m) - [v][t] string - Unique identifier
	* "name" (o) - [v][t] string
	* "expect" (o) [o]
		* "sw" (o) [v] - by default 9000
		* "data" (o) [ao]
			* "value" (m) - [v] - value to compare with output
			* "format" (m) - [v][t] string - format of the "value" format
			* "value_exec_id" (o) - [v][t] string - Unique identifier
			* "comparison" (o) - [v][t] string - by default "eq"
			
	* "stop_condition" (o)
		* "timeout" (o) - [v][t] long number - by default -1
		* "repeat_count" (o) - [v][t] integer number - by default 1
		* "on_exception" (o) - [v][t] boolean - by default 
		
	* "input" (mt) {AS} - [o] 
		* "value" (m) - [v] - value to compare with output
		* "format" (m) - [v][t] string - format of the "value" format
		
	* "jump_to_exec_id" - (mt) {R} - [v][t] string
	* "delay" - (mt) {W} - [v][t] long number

* "exec_que" (m) [av]
	* [v][t] string - ordered Unique identifier
```
---

## Keywords

#### keys

* "commands"
	* "cmd"
	* "exec_id"
	* "name"
	* "expect"
		* "sw"
		* "data"
			* "value"
			* "format"
			* "comparison"
	* "stop_condition"
		* "timeout"
		* "repeat_count"
		* "on_exception"
	* "input"
		* "value"
		* "format"
	* "jump_to_exec_id"
	* "delay"
* "exec_que"

#### value types
In JSON, values must be one of the following data types:

* a string 
* a number
* an object (JSON object)
* an array
* a boolean
* null

In script expected data and input/output value types are specified as 

* "null" - translates to null / nothing
* "string" - a JSON string
* "integer" - a JSON number representing Java/Kotlin Integer
* "long"  - a JSON number representing Java/Kotlin Long
* "bytes" - can be ued only with reference to previous or default value
* "hexbytes" - a JSON string representing hexadecimal value - not Tested
* "datetime" - date and time in seconds from epoch - formatted as : 
* "json" - an JSON object
* "http" - an JSON object representing HTTP Request - ```{"url": http://example.com/dest, "data":"any payload","headers": "[{"Content-Type", "application/json"}]",}```

#### APDU command types
*		 "READ_BUFFER"
*                "READ_SAMPLING"
*                "SET_SAMPLING"
*                "READ_CLOCK"
*                "SET_CLOCK"
*                "READ_LOW_WATER"
*                "SET_LOW_WATER"
*                "READ_HIGH_WATER"
*                "SET_HIGH_WATER"
*                "READ_PUB_KEY"
*                "SET_GEN_KEYS"
*                "READ_ETH_ADD"
*                "READ_ACCOUNT"
*                "SET_ETH_SEED"
*                "READ_ASSET"
*                "SET_ASSET"
*                "SET_EVENT_START"
*                "READ_EVENT_LEN"
*                "READ_EVENT_DATA"
*                "SET_EVENT_FINISH"
*                "SET_MODEM_SETUP"

#### L2 Preferences command types
*		 "PREFS_ASSET"
*		 "PREFS_ACCOUNT"
*		 "PREFS_TOKEN"
*		 "PREFS_SECRET"
*		 "PREFS_PUB_KEY"

#### Validation command types
*		 "VALID_L1_EVENT"
*		 "VALID_ASSET"
*		 "VALID_ETH_ADD"
*		 "VALID_TOKEN"
*		 "VALID_SECRET"
*		 "VALID_PUB_KEY"

#### HTTP command types
*		 "POST_ACCOUNT"
*		 "POST_EVENT"


---
## Examples 
Simple description of each command block is in field "name" (optional). Commands does not need to be ordered in "commands", but for better readability they are in most of the examples.

### Simple example
* [Set clock twice](https://gitlab.com/crayonic/Ambrosus_L2_Android/blob/master/app/src/main/res/raw/example.json) 

###  Values setup and validation example
* [Setup AMB values on L2 ](https://gitlab.com/crayonic/Ambrosus_L2_Android/blob/master/app/src/main/res/raw/amb_values_script.json)
* [Setup AMB values on L2 with validation ](https://gitlab.com/crayonic/Ambrosus_L2_Android/blob/master/app/src/main/res/raw/amb_values_with_validation_script.json)

###  Crypto examples
* [Provisioning of L1](https://gitlab.com/crayonic/Ambrosus_L2_Android/blob/master/app/src/main/res/raw/provisioning_script.json) 
* [Read AMB Event process](https://gitlab.com/crayonic/Ambrosus_L2_Android/blob/master/app/src/main/res/raw/event_script.json) 